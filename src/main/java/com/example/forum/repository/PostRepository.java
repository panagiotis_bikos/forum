package com.example.forum.repository;


import com.example.forum.entity.Post;
import com.example.forum.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {


//    @Query("SELECT p FROM Post p " +
//            "INNER JOIN Users u " +
//            "ON p.users.userId=u.userId " +
//            "WHERE u.userId = ?1")
//    List<Post> findPostByUserId(Users users);

    List<Post> findPostByUsers(Users users);

    Post findPostByPostId(long id);
}
