package com.example.forum.repository;

import com.example.forum.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UsersRepository extends JpaRepository<Users, Long>, PagingAndSortingRepository<Users,Long> {

    Users findByUserId(long id);

    Page<Users> findAll(Pageable pageable);
}
