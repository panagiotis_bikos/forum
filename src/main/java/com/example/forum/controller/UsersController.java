package com.example.forum.controller;

import com.example.forum.entity.Post;
import com.example.forum.entity.Users;
import com.example.forum.repository.PostRepository;
import com.example.forum.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("")
    public @ResponseBody List<Users> getAllUsers(){
        return usersRepository.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Users getUser(@PathVariable(name = "id") Long id){

       return usersRepository.findByUserId(id);

    }

//    @GetMapping("/test")
//    public Page<Users> getPage(@RequestParam Optional<Integer> page){
//        return usersRepository.findAll(new PageRequest(0,2,Sort.Direction.ASC));
//
//    }


}
