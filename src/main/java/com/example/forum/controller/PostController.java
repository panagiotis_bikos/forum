package com.example.forum.controller;

import com.example.forum.entity.Post;
import com.example.forum.entity.Users;
import com.example.forum.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/users")
public class PostController {

    @Autowired
    private PostRepository postRepository;

    @GetMapping("/{userId}/posts")
    public @ResponseBody List<Post> getPosts(@PathVariable(name = "userId") Users userId, Model model){
        return postRepository.findPostByUsers(userId);

    }

    @GetMapping("/posts/{id}")
    public @ResponseBody Post getPost(@PathVariable(name = "id") Long id){

        return postRepository.findPostByPostId(id);

    }
}
